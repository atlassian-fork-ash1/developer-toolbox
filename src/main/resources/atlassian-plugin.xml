<?xml version="1.0" encoding="UTF-8"?>

<atlassian-plugin key="${project.groupId}.${project.artifactId}" name="${project.name}" plugins-version="2">
    <plugin-info>
        <description>${project.description}</description>
        <version>${project.version}</version>
        <vendor name="${project.organization.name}" url="${project.organization.url}"/>
    </plugin-info>
    <resource type="i18n" name="i18n" location="developer-toolbox"/>

    <!--
        Imported SAL/TemplateRenderer components for basic functionality
     -->
    <component-import key="template-renderer" interface="com.atlassian.templaterenderer.velocity.one.six.VelocityTemplateRenderer" />
    <component-import key="application-properties" interface="com.atlassian.sal.api.ApplicationProperties" />
    <component-import key="userManager" interface="com.atlassian.sal.api.user.UserManager" />
    <component-import key="loginUriProvider" interface="com.atlassian.sal.api.auth.LoginUriProvider" />
    <component-import key="dynamicWebInterfaceManager" interface="com.atlassian.plugin.web.api.DynamicWebInterfaceManager" />

    <!--
        Platform components accessor
     -->
    <component key="platformComponentsAccessor"
               class="com.atlassian.devrel.plugin.PlatformComponentsImpl">
        <interface>com.atlassian.devrel.plugin.PlatformComponents</interface>
    </component>

    <component key="pluginChecks" class="com.atlassian.devrel.checks.PluginChecks" />

    <!--
        Customized Velocity context items
     -->
    <component key="textUtils" class="com.atlassian.devrel.util.TextUtils"/>
    <template-context-item key="textUtilsContextItem" component-ref="textUtils"
                           context-key="textUtils" name="TextUtils Context Item"/>
    <template-context-item key="applicationPropertiesContextItem" component-ref="application-properties"
                           context-key="applicationProperties" name="Application Properties Context Item"/>

    <!--
        Sandbox servlet
    -->
    <servlet name="Sandbox Servlet" key="sandbox-servlet"
             class="com.atlassian.devrel.servlet.SandboxServlet">
        <url-pattern>/dev-sandbox</url-pattern>
        <url-pattern>/dev-sandbox/*</url-pattern>
        <url-pattern>/empty</url-pattern>
        <url-pattern>/empty/*</url-pattern>
    </servlet>

    <!--
        MVC servlets
     -->
    <servlet name="Homepage Servlet" key="homepage-servlet"
             class="com.atlassian.devrel.servlet.HomepageServlet">
        <description>Serves the toolbox home page HTML.</description>
        <url-pattern>/developer-toolbox</url-pattern>
    </servlet>

    <!--
        Web resources for the toolbox homepage
     -->
    <web-resource key="homepage-resources">
        <dependency>com.atlassian.auiplugin:ajs</dependency>
        <dependency>com.atlassian.auiplugin:aui-button</dependency>
        <dependency>com.atlassian.auiplugin:aui-toolbar2</dependency>
        <dependency>com.atlassian.plugins.atlassian-plugins-webresource-plugin:context-path</dependency>
        <dependency>com.atlassian.devrel.developer-toolbox-plugin:quickreload-toggle-button</dependency>
        <resource name="images/" type="download" location="/assets/homepage/images/"/>
        <resource name="homepage.css" type="download" location="/assets/homepage/css/homepage.css"/>
        <resource name="homepage.js" type="download" location="/assets/homepage/js/homepage.js"/>
    </web-resource>

    <!--
        Web items for displaying the Dev Toolbox link in the product administration consoles.
     -->
    <web-item name="Developer Toolbox" i18n-name-key="developer-toolbox.name-jira" key="developer-toolbox-link-jira"
              section="top_system_section/advanced_section" weight="1000"
              application="jira">
        <description key="developer-toolbox.description">Developer Toolbox</description>
        <label key="developer-toolbox.label"/>
        <link linkId="developer-toolbox-link">/plugins/servlet/developer-toolbox</link>
        <condition class="com.atlassian.jira.plugin.webfragment.conditions.UserIsAdminCondition"/>
    </web-item>

    <web-item name="Developer Toolbox" i18n-name-key="developer-toolbox.name.jira" key="user-profile-developer-toolbox-link-jira"
              section="system.user.options/jira-help"
              application="jira">
        <description key="developer-toolbox.description">Developer Toolbox</description>
        <label key="developer-toolbox.label"/>
        <link linkId="developer-toolbox-link">/plugins/servlet/developer-toolbox</link>
        <condition class="com.atlassian.jira.plugin.webfragment.conditions.UserIsAdminCondition"/>
    </web-item>
    <!-- END web items for displaying REST Browser link in admin consoles -->

    <web-resource key="dt-toolbar-namespace">
        <resource name="dt-toolbar-namespace.js" type="download" location="assets/js/namespace.js"/>
    </web-resource>

    <web-resource key="dt-toolbar">
        <dependency>com.atlassian.auiplugin:ajs</dependency>
        <dependency>com.atlassian.devrel.developer-toolbox-plugin:dt-toolbar-namespace</dependency>
        <dependency>com.atlassian.devrel.developer-toolbox-plugin:quickreload-toggle-button</dependency>
        <resource name="toolbar.js" type="download" location="assets/js/toolbar.js"/>
        <resource name="toolbar.css" type="download" location="/assets/css/toolbar.css"/>
        <resource name="ajax-loader.gif" type="download" location="/assets/homepage/images/ajax-loader.gif"/>
        <context>atl.general</context>
        <context>atl.admin</context>
        <context>atl.userprofile</context>
        <context>atl.popup</context>
        <context>atl.livereload</context>
        <context>dev.toolbar</context>
        <condition class="com.atlassian.devrel.condition.DevModeCondition"
                   class2="com.atlassian.devrel.condition.DevModeUrlReadingCondition" />
    </web-resource>

    <web-resource key="quickreload-toggle-button">
        <dependency>com.atlassian.devrel.developer-toolbox-plugin:dt-toolbar-namespace</dependency>
        <dependency>com.atlassian.plugins.atlassian-plugins-webresource-plugin:context-path</dependency>
        <dependency>com.atlassian.auiplugin:ajs</dependency>
        <dependency>com.atlassian.auiplugin:aui-toggle</dependency>
        <dependency>com.atlassian.auiplugin:toggle</dependency>
        <dependency>com.atlassian.auiplugin:aui-tooltips</dependency>
        <dependency>com.atlassian.auiplugin:aui-tooltip</dependency>
        <dependency>com.atlassian.auiplugin:tooltip</dependency>
        <resource name="qr-toggle.css" type="download" location="features/quickreload/quickreload-toggle.css"/>
        <resource name="qr-toggle.js" type="download" location="features/quickreload/quickreload-toggle.js"/>
    </web-resource>

    <resource name="images/" type="download" location="assets/images/"/>

    <servlet name="Toolbar Servlet"
             key="toolbar-servlet" class="com.atlassian.devrel.servlet.ToolbarServlet">
        <url-pattern>/dev-toolbar</url-pattern>
    </servlet>

    <web-item name="Sandbox Servlet Link" key="sandbox-servlet-toolbar-item"
              section="dev-toolbar-menu" weight="1">
        <tooltip key="sandbox-servlet.description">Sandbox Servlet</tooltip>
        <label key="sandbox-servlet.label"/>
        <link linkId="dt-sandbox-servlet">/plugins/servlet/empty</link>
    </web-item>

    <web-item name="Highlight i18n" key="highlight-i18n-toolbar-item"
              section="dev-toolbar" weight="10">
        <tooltip key="highlight-i18n.description">Highlight i18n</tooltip>
        <label key="highlight-i18n.label"/>
        <link linkId="dt-highlight-i18n"/>
        <param name="type" value="image"/>
        <condition class="com.atlassian.devrel.condition.HighlightI18NEnabledCondition"/>
    </web-item>

    <web-item name="Show Stash Web Fragments" key="stash-web-fragments-toolbar-item"
              section="dev-toolbar" weight="10" application="stash">
        <tooltip key="show-stash-web-fragements.description">Highlight Stash Web Fragments</tooltip>
        <label key="show-stash-web-fragments.label"/>
        <link linkId="dt-bitbucket-web-fragments"/>
        <param name="type" value="image"/>
    </web-item>

    <web-item name="Show Bitbucket Web Fragments" key="bitbucket-web-fragments-toolbar-item"
              section="dev-toolbar" weight="10" application="bitbucket">
        <tooltip key="show-bitbucket-web-fragments.description">Highlight Bitbucket Web Fragments</tooltip>
        <label key="show-bitbucket-web-fragments.label"/>
        <link linkId="dt-bitbucket-web-fragments"/>
        <param name="type" value="image"/>
    </web-item>

</atlassian-plugin>
