package com.atlassian.devrel.servlet;

import com.atlassian.webresource.api.UrlMode;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;
import com.atlassian.webresource.api.assembler.WebResourceAssemblerFactory;
import com.atlassian.webresource.api.assembler.WebResourceSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

public class SandboxServlet extends HttpServlet {
    private static final String WRM_REQUIRE_RESOURCE_KEY = "com.atlassian.plugins.atlassian-plugins-webresource-plugin:web-resource-manager";

    private final PageBuilderService pageBuilderService;
    private final WebResourceAssemblerFactory assemblerFactory;

    public SandboxServlet(
            WebResourceAssemblerFactory factory,
            PageBuilderService pageBuilderService
    ) {
        this.assemblerFactory = factory;
        this.pageBuilderService = pageBuilderService;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final boolean includeSuperbatch = bool(req.getParameter("super"), false);
        final boolean allowAsync = bool(req.getParameter("async"), false);
        final List<String> contexts = safe(req.getParameterValues("context"));
        final List<String> resources = safe(req.getParameterValues("resource"));

        if (allowAsync) {
            resources.add(WRM_REQUIRE_RESOURCE_KEY);
        }

        // Set response headers and metadata
        resp.setContentType("text/html;charset=UTF-8");

        // Open the response.
        resp.getWriter().write(
                "<!doctype html>" +
                        "<html>" +
                        "<head>" +
                        "    <title>An empty page</title>" +
                        "</head>" +
                        "<body>" +
                        "    <p>You can use this page to test UI features in isolation!</p>" +
                        "    <p>Try the following things:</p>" +
                        "    <dl>" +
                        "        <dt>Load the superbatch</dt>" +
                        "        <dd><a href='?super=true'>Set <var>?super=true</var> in the URL</a></dd>" +

                        "        <dt>Load <var>WRM.require</var></dt>" +
                        "        <dd><a href='?async=true'>Set <var>?async=true</var> in the URL</a></dd>" +

                        "        <dt>Load a specific web-resource</dt>" +
                        "        <dd>Set <var>resource=[webresource-key]</var> in the URL</var></dd>" +

                        "        <dt>Load a specific web-resource context</dt>" +
                        "        <dd>Set <var>context=[webresource-key]</var> in the URL</var></dd>" +

                        "    </dl>"
        );
        resp.getWriter().flush();

        final WebResourceAssembler assembler = getAssembler(includeSuperbatch);

        for (String ctx : contexts) {
            assembler.resources().requireContext(ctx);
        }
        for (String res : resources) {
            assembler.resources().requireWebResource(res);
        }

        // Drain anything that should be loaded.
        final WebResourceSet webResourceSet = assembler.assembled().drainIncludedResources();
        final Writer webResourceWriter = new StringWriter();
        webResourceSet.writeHtmlTags(webResourceWriter, UrlMode.AUTO);
        final String webResourceHtml = webResourceWriter.toString();

        if (webResourceHtml.length() > 0) {
            resp.getWriter().write(webResourceHtml);
            resp.getWriter().write("<p>The requested resources have been loaded!</p>");
        }

        if (allowAsync) {
            resp.getWriter().write(
                    "    <p>There should be a <code>WRM.require</code> function on the page now.</p>\n" +
                            "    <p>Try running something like:</p>\n" +
                            "    <pre><code>WRM.require(['com.atlassian.auiplugin:ajs'], function() {\n" +
                            "        console.log('loaded', {AJS});\n" +
                            "    })</code></pre>\n"
            );
        }
        resp.getWriter().flush();

        resp.getWriter().write("</body></html>");
        resp.getWriter().close();
    }

    private WebResourceAssembler getAssembler(final boolean includeSuperbatch) {
        if (null != assemblerFactory) {
            return assemblerFactory.create().includeSuperbatchResources(includeSuperbatch).build();
        } else {
            return pageBuilderService.assembler();
        }
    }

    private boolean bool(final String val, final boolean defaultVal) {
        if (val == null) {
            return defaultVal;
        }
        return Boolean.parseBoolean(val);
    }

    private List<String> safe(final String[] input) {
        List<String> results = new ArrayList<String>();
        if (input != null) {
            for (String val : input) {
                if (val != null && val.length() > 0) {
                    results.add(val);
                }
            }
        }
        return results;
    }
}
