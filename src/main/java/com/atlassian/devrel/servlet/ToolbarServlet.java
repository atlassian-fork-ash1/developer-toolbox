package com.atlassian.devrel.servlet;

import com.atlassian.devrel.checks.PluginChecks;
import com.atlassian.plugin.web.api.DynamicWebInterfaceManager;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.google.common.collect.Maps;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;

public class ToolbarServlet extends HttpServlet {
    private static final String QR_PLUGIN_KEY = "com.atlassian.labs.plugins.quickreload.reloader";
    private static final String TOOLBAR_TEMPLATE = "/templates/toolbar.vm";

    private TemplateRenderer renderer;
    private ApplicationProperties applicationProperties;
    private DynamicWebInterfaceManager webInterfaceManager;
    private PluginChecks checks;

    public ToolbarServlet(TemplateRenderer renderer, ApplicationProperties applicationProperties,
                          DynamicWebInterfaceManager webInterfaceManager, PluginChecks checks) {
        this.webInterfaceManager = webInterfaceManager;
        this.renderer = checkNotNull(renderer, "renderer");
        this.applicationProperties = checkNotNull(applicationProperties, "applicationProperties");
        this.checks = checks;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Map<String, Object> context = Maps.newHashMap();
        Map<String, Object> condition = Maps.newHashMap();

        context.put("navBarItems", webInterfaceManager.getDisplayableWebItems("dev-toolbar", condition));
        context.put("navBarMenuItems", webInterfaceManager.getDisplayableWebItems("dev-toolbar-menu", condition));
        context.put("app", applicationProperties);
        context.put("sdkVersion", System.getProperty("atlassian.sdk.version", "3.7 or earlier"));
        context.put("devToolboxVersion", checks.getDevtoolboxVersion());
        context.put("quickreloadVersion", checks.getQuickReloadVersion());
        resp.setContentType("text/html;charset=utf-8");
        renderer.render(TOOLBAR_TEMPLATE, context, resp.getWriter());
    }

    private String getAppName() {
        return applicationProperties.getDisplayName().toLowerCase();
    }

}
