package com.atlassian.devrel.condition;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;
import com.atlassian.plugin.webresource.condition.UrlReadingCondition;

import java.util.Map;

import static com.atlassian.devrel.condition.DevModeUrlReadingCondition.isDevToolbarEnabled;

/**
 * @deprecated replaced by {@link DevModeUrlReadingCondition}. Can be removed after all products support
 * {@link UrlReadingCondition}, introduced in {@code atlassian-plugins-webresource-3.0.7}
 */
@Deprecated
public class DevModeCondition implements Condition
{
    @Override
    public void init(Map<String, String> stringStringMap) throws PluginParseException {
    }

    @Override
    public boolean shouldDisplay(Map<String, Object> stringObjectMap) {
        return isDevToolbarEnabled();
    }
}
